import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_study.settings')
django.setup()