from django.test import TestCase
from django.utils import timezone
from django.db import DatabaseError

from ci_cd.models.person import Person


class UserModelTestCase(TestCase):

    def setUp(self):
        self.person = Person(
            first_name='Jonas',
            second_name='Jonas',
            birthday=timezone.now(),
        )
        self.person.save()

    def test_first_name_not_null(self):
        self.person.first_name = None
        with self.assertRaises(DatabaseError):
            self.person.save()

    def test_second_name_not_null(self):
        self.person.second_name = None
        with self.assertRaises(DatabaseError):
            self.person.save()

    def test_birthday_not_null(self):
        self.person.birthday = None
        with self.assertRaises(DatabaseError):
            self.person.save()
